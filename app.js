function filterBy(arr, dataType) {
    return arr.filter(item => typeof item !== dataType);
  }
  

  const inputArray = ['hello', 'world', 23, '23', null];
  const filteredResult = filterBy(inputArray, 'string');
  console.log(filteredResult);